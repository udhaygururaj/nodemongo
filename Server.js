const express = require('express');
const app = express();
const bodyParser= require('body-parser')
const MongoClient = require('mongodb').MongoClient

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/'));
var db;

MongoClient.connect('mongodb://127.0.0.1:27017/assetmanager', (err, database) => {
	  if (err) return console.log(err)
	  db = database
  	app.listen(3000, function() {
  		console.log('listening on 3000')
	})
})

app.get('/', function (request, response) {
 response.sendFile(__dirname + '/index.html')
})

app.post('/assetregister', (req, res) => {
 
  db.collection('register').save(req.body, (err, result) => {
    if (err) return console.log(err)

    console.log('saved to database')
    res.redirect('/')
  })
})